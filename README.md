<div align="center">

  # dwmbellish (beautified dwm)

</div>

![Screenshot1](https://gitlab.com/barryboy/dwmbellish/-/raw/main/.gitlab/screenshots/screen1.png)

![Screenshot2](https://gitlab.com/barryboy/dwmbellish/-/raw/main/.gitlab/screenshots/screen2.png)

![Screenshot3](https://gitlab.com/barryboy/dwmbellish/-/raw/main/.gitlab/screenshots/screen3.png)

![Screenshot4](https://gitlab.com/barryboy/dwmbellish/-/raw/main/.gitlab/screenshots/screen4.png)

## Installation

```bash
git clone https://gitlab.com/barryboy/dwmbellish.git --depth 1  ~/.config
cd .config/dwmbellish
sudo make clean install
```

For autostart there are two files that can be customized:
autostart-blocking.sh and autostart.sh, the first is run before dwm is started and dwm wont start till it finished executing, the latter runs commands after dwm started.
Make scripts at ~/.local/share/dwm/autostart.sh and ~/.local/share/dwm/autostart_blocking.sh

## Making Changes And Compiling It Again

```bash
cd ~/.config/dwmbellish
rm config.h & make
sudo make clean install
```

## Patches
+ ActiveTagIndicatorBar					- shows bar on tags with windows, full bar for current window
+ AutoStart                             - autostarts 2 files at startup
+ BarHeight                             - makes it easy to set pixel height of bar, disconnected from fontsize
+ FakeFullscreenClient                  - adds keybind for fullscreening window inside window space
+ FixBorders                            - makes borders of windows not transparent
+ FocusFullscreen						- makes switching windows from fullscreen like in monocle layout
+ FullGaps                              - adds gaps inbetween window
+ PreserveonRestart                     - preserves windows and windows location on restart
+ RainbowTags							- set color of every active tag
+ RestartSig                            - adds restart keybind
+ RotateStack                           - can rotate windows in the stack
+ StatusButton                          - NOT IMPLEMENTED / adds customizable button to bar
+ Systray                               - adds systray support
+ LG3D                                  - not really a patch, change WMNAME to LG3D to fix some java apps

## Keybindings
**MODKEY = Ctrl**
- MODKEY,                       Return, spawn,          terminal
- MODKEY,                       o,      spawn,          rofi
- MODKEY,                       b,      spawn,          firefox
- MODKEY,                       e,      spawn,          emacs
- MODKEY|ShiftMask,             l,      spawn,          powermenu.sh
- MODKEY,                       n,      spawn,          librewolf
- MODKEY|ShiftMask,             b,      togglebar,      
- MODKEY|ShiftMask,             j,      rotatestack,    {.i = +1 } 
- MODKEY|ShiftMask,             k,      rotatestack,    {.i = -1 } 
- MODKEY,                       j,      focusstack,     {.i = +1 } 
- MODKEY,                       k,      focusstack,     {.i = -1 } 
- MODKEY,                       i,      incnmaster,     {.i = +1 } 
- MODKEY,                       d,      incnmaster,     {.i = -1 } 
- MODKEY,                       h,      setmfact,       {.f = -0.05}
- MODKEY,                       l,      setmfact,       {.f = +0.05}
- MODKEY|ShiftMask,             Return, zoom
- MODKEY,                       Tab,    view
- MODKEY|ShiftMask,             c,      killclient
- MODKEY,                       t,      setlayout,      {.v = &layouts[0]}
- MODKEY,                       m,      setlayout,      {.v = &layouts[2]}
- MODKEY,                       f,      setlayout,      {0}
- MODKEY|ShiftMask,             f,      togglefloating
- MODKEY|ShiftMask,             space,  togglefakefullscreen
- MODKEY,                       space,  togglefullscreen
- MODKEY,                       0,      view,           {.ui = ~0 }
- MODKEY|ShiftMask,             0,      tag,            {.ui = ~0 }
- MODKEY,                       comma,  focusmon,       {.i = -1 }
- MODKEY,                       period, focusmon,       {.i = +1 }
- MODKEY|ShiftMask,             comma,  tagmon,         {.i = -1 }
- MODKEY|ShiftMask,             period, tagmon,         {.i = +1 }
- MODKEY,                       minus,  setgaps,        {.i = -1 }
- MODKEY,                       equal,  setgaps,        {.i = +1 }
- MODKEY|ShiftMask,             equal,  setgaps,        {.i = 0  }
- MODKEY|ShiftMask,             q,      quit,         
- MODKEY|ShiftMask,             r,      restart,



